# SWIFFT Projects


These are the SWIFFT projects completed by Intern Patrick Woods in the summer 17'


## Getting Started


In this folder contains a SWIFFT Java implementation, OCaml implementation, SWIFFTX on
PIC32, SWIFFTX DFT function with AVX, and extras.


### Java Implementation


- Runs SWIFFT in 16 milliseconds for input of 2048 bits
- swifft is faster/smaller/more readable than swifft_o, however the test file uses swifft_o for comparison
[swifft.java](swifft.java), [swifft_o.java](swifft_o.java), [Test.java](Test.java)

```
To compile: javac swifft.java swifft_o.java Test.java
To run: java Test
```


### OCaml Implementation


- Runs SWIFFT in 0.0002 milliseconds for input of 2048 bits
- Potential speedups could be gained using a) lookup tables b) making the swifft_ntt mile a single/long recursive call
[NTT.ml](NTT.ml), [swifft_ntt.ml](swifft_ntt.ml), [testswifft.ml](testswifft.ml)

```
To compile: ocamlc unix.cma -o ft  NTT.ml swifft_ntt.ml testswifft.ml
To run: ./ft
```


### SWIFFT_X on PIC32


You will need MPLABX_IDE and 2 drivers that Mark Wilson knows about. It currently runs
in the simulator (need to activate UART Port 2 to see output), but not on the hardware.
Within the IDE, after you open the project, the project may be named "anotherone.X"
[swifft_x.X](swifft_x.X)

```
To run: select "Debug Project (swifft_x.X OR anotherone.X)" within the Deubug drop-down window at the top 
```


### DFT with AVX


- Cut and paste this DFT method over the DFT method in the Reference Implementation, cuts 10% of cycles
- Add -mavx2 -mfma to the makefile
[DFT_AVX.c](DFT_AVX.c)

```
In the makefile: 
sha3: Tester.c SHA3.c SWIFFTX.c SWIFFTX.h SHA3.h
	${CC} -O4 -DSYS_STDINT ${OPT} -msse2 -mavx2 -mfma SHA3.c SWIFFTX.c Tester.c -o sha3
produceiv: ProduceRandomIV.c SHA3.c SWIFFTX.c SWIFFTX.h SHA3.h
	${CC} -O4 -DSYS_STDINT ${OPT} -msse2 -mavx2 -mfma SHA3.c SWIFFTX.c ProduceRandomIV.c -o produceiv
```


### Extras


IDQ Proposal: [IDQ Project Proposal](IDQ Project Proposal.pdf)
Final Powerpoint: [PPT](SWIFFT Summer Internship.pdf)
Daily: [Daily](Daily.pdf)


## Acknowledgments

* Thank you Roberta, Rino, Andy, John Wayde, John Petro, Jeff, Paul, and Mark for the amazing opportunity and experience! 