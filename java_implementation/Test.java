//To compile: javac swifft_o.java swifft.java Test.java
//To run: java Test

import java.util.Random;

public class Test {

	public static void main(String[] args) {

		Random rn = new Random();
		int[] message = new int[64];
		int[][] matrix = new int[1][64];
		message[1] = 1;
		message[5] = 1;
		message[34] = 1;
		message[56] = 1;

		System.out.println("");
		System.out.println("Input Matrix: ");
		System.out.println("");
		for (int i = 0; i < 64; i++)
		{
			matrix[0][i] = rn.nextInt(257);
			System.out.print(matrix[0][i] + " ");
		}
		System.out.println("");
		long start = System.nanoTime();
		int[] z1 = swifft_o.swifft(message, matrix);
		System.out.println(System.nanoTime() - start);
		start = System.nanoTime();
		int[] z2 = swifft.swifft(message, matrix);
		System.out.println(System.nanoTime() - start);
		System.out.println("");
		System.out.println("");
		System.out.println("");
		System.out.println("Input Message: ");
		System.out.println("");
		for (int i =0; i < 64; i++)
		{
			System.out.print(message[i] + " | ");
		}
		System.out.println("");
		System.out.println("");
		System.out.println("z (comp): ");
		System.out.println("");
		for(int i = 0; i < 64; i++)
		{
			System.out.print(z1[i] + ":" + z2[i] + " | ");
			message[i] = rn.nextInt(2);
		}

		int[] z = swifft_o.swifft(message, matrix);
		System.out.println("");
		System.out.println("");
		System.out.println("New Message: ");
		System.out.println("");
		for(int i = 0; i < 64; i++)
		{
			System.out.print(message[i] + " | ");
		}

		System.out.println("");
		System.out.println("");
		System.out.println("New z: ");
		System.out.println("");
		for(int i = 0; i < 64; i++)
		{
			System.out.print(z[i] + " | ");
		}

		System.out.println("");
		System.out.println("");
		System.out.println("New Matrix: ");
		System.out.println("");
		int[][] matrix2 = new int[3][64];
		for (int i = 0; i < 3; i++)
		{
			for(int j = 0; j < 64; j++)
			{
				matrix2[i][j] = rn.nextInt(257);
				System.out.print(matrix2[i][j] + " | ");
			}
		}

		System.out.println("");
		System.out.println("");
		System.out.println("Final z: ");
		System.out.println("");
		z = swifft_o.swifft(message, matrix2);
		for(int i = 0; i < 64; i++)
		{
			System.out.print(z[i] + " | ");
		}

		System.out.println("");
		System.out.println("");
		System.out.println("Benchmarks for input message of length 64*32: ");
		System.out.println("");
		int[] bmessage = new int[64*32];
		int[][] bmatrix = new int[32][64];
		for(int p = 0; p < 64*32; p++)
		{
			bmessage[p] = rn.nextInt(2);
		}

		for (int p = 0; p < 32; p++)
		{
			for (int g = 0; g < 64; g++)
			{
				bmatrix[p][g] = rn.nextInt(257);
			}
		}

		long b = System.nanoTime();
		z = swifft_o.swifft(bmessage, bmatrix);
		b = System.nanoTime() - b;
		System.out.println("SWIFFT_O: " + b);
		for (int i = 0; i < 64; i++)
		{
			System.out.print(z[i] + " | ");
		}
		b = System.currentTimeMillis();
		z = swifft.swifft(bmessage, bmatrix);
		b = System.currentTimeMillis() - b;
		System.out.println("");
		System.out.println("SWIFFT: " + b);
		for (int i = 0; i < 64; i++)
		{
			System.out.print(z[i] + " | ");
		}
	}
	
}


//clean up this part that matches the OCaml parameters, then write a clean
//display of the roots of unity, all 1's, and a couple randoms