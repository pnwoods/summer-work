/* 
	*  Patrick Woods
	*  07/24/2017
	*  Envieta Systems LLC
	*  SWIFFT Java Implementation with Optimizations

	This program contains several methods that are used together to form the SWIFFT compression function, 
	which is called by the final static method swifft. The final method swifft takes 2 parameters, a binary inputmessage, 
	and a matrix of at least (inputmessage.length)/64 x 64. The binary message can be any length, 
	however if the length is not divisable by 64, then 0's will be padded to the end of it until it is divisable by 64. 
	The function outputs the 64 computed 'z' values in modulo 257. Modular arithmetic is used thoroughly during the calculation 
	to avoid an int overflow. 

 */

	//SWITCH

import java.util.Arrays;

public class swifft {

	// This method takes an array, and an int. If the int is odd then the method returns the odd elements (likewise evens).
	private static int[] filter (int x, int[] arr)
	{
		int newarr[] = new int[(arr.length / 2)]; //array to be returned

		if (x % 2 == 0) //even protocol
		{
			for (int i = 0; i < newarr.length; i++)
			{
				newarr[i] = arr[2*i];
			}
		}

		else //odd protocol
		{
			for (int i = 0; i < newarr.length; i++)
			{
				newarr[i] = arr[(2*i)+1];
			}
		}

		return newarr;
	}

	// This method perfroms the ntt operation on a 64 length array; computes A(omega)
	private static int ntt(int[] arr, int omega)
	{
		if (arr.length > 1) //recursive step
		{
			int even[] = filter (2, arr);
			int odd[] = filter (1, arr);
			return ((ntt(even, (omega * omega) % 257) + (omega*(ntt(odd, (omega * omega) % 257)))) % 257);
		}

		else //base case
		{
			return (arr[0] * omega) % 257;
		}
	}

	// This is the method that will be called to perform SWIFFT. It pads the array if necessary, checks the matrix,
	// and returns z in an array
	public static int[] swifft(int[] arr, int[][] mat)
	{
		if ((arr.length % 64) != 0) //performs array padding
		{
			int[] newarr = new int[arr.length + (64 - (arr.length % 64))];
			for (int i = 0; i < arr.length; i++)
			{
				newarr[i] = arr[i];
			}
			return swifft(newarr, mat);
		}
		
		try {  
				int x = mat[(arr.length / 64)-1][63]; //check to see if matrix is valid
		}

		catch (IndexOutOfBoundsException e) {
				System.out.println("Matrix is too small. Must at least be Matrix[Array.length / 64 - 1][64]");
				System.exit(1);
		}

		int[] bitreverse = {0,32,16,48,8,40,24,56,4,36,20,52,12,44,28,60,2,34,18,50,10,42,26,58,6,38,22,54,14,46,
							30,62,1,33,17,49,9,41,25,57,5,37,21,53,13,45,29,61,3,35,19,51,11,43,27,59,7,39,23,55,15,
								47,31,63}; //Pre-computed bitreversal values; index 2 value becomes index 32 value

		int[] w = {1,42,222,72,197,50,44,49,2,84,187,144,137,100,88,98,4,168,117,31,17,200,176,196,8,79,234,62,34,
					143,95,135,16,158,211,124,68,29,190,13,32,59,165,248,136,58,123,26,64,118,73,239,15,116,246,52,128,
						236,146,221,30,232,235,104,256}; //Pre-computed values of w^0 - w^64 mod 257

		int[] w2 = {1,222,197,44,2,187,137,88,4,117,17,176,8,234,34,95,16,211,68,190,32,165,136,123,64,73,15,246,128,
					146,30,235,256,35,60,213,255,70,120,169,253,140,240,81,249,23,223,162,241,46,189,67,225,92,121,134,193,
						184,242,11,129,111,227,22}; //Pre-computed values of w_squared^0 - w_squared^64 mod 257

		int[] temp_arr = new int[64]; //stores subsection of input message
		int[] br_arr = new int[64]; //stores array post bitreversal/premultiply
		int[] z = new int[64]; //this will be returned at the end

		//SWIFFT
		for (int i = 0; i < (arr.length / 64); i++) //loops through length of message
		{
			temp_arr = Arrays.copyOfRange(arr, (64*i), ((i*64)+64)); //grab current 64 length section of the message 

			for (int j = 0; j < 64; j++)
			{
				br_arr[j] = temp_arr[bitreverse[j]]; //bitreverse
				br_arr[j] = (br_arr[j] * w[j] % 257); //premultiply
			} 

			for (int j = 0; j < 64; j++)
			{
				z[j] = ((z[j] + (mat[i][j] * ntt(br_arr, w2[j]))) % 257); //compute z
			}
		}

		return z; 
	}
}