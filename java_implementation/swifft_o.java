/* 
	*  Patrick Woods
	*  07/24/2017
	*  Envieta Systems LLC
	*  SWIFFT Java Implementation

	This program contains several methods that are used together to form the SWIFFT compression function, 
	which is called by the final static method swifft. The final method swifft takes 2 parameters, a binary inputmessage, 
	and a matrix of at least (inputmessage.length)/64 x 64. The binary message can be any length, 
	however if the length is not divisable by 64, then 0's will be padded to the end of it until it is divisable by 64. 
	The function outputs the 64 computed 'z' values in modulo 257. Modular arithmetic is used thoroughly during the calculation 
	to avoid an int overflow. 

 */

import java.util.Arrays;

public class swifft_o {

	// This method takes an array, and an int. If the int is odd then the method returns the odd elements (likewise evens).
	private static int[] filter (int x, int[] arr)
	{
		int newarr[] = new int[(arr.length / 2)]; //array to be returned

		if (x % 2 == 0) //even protocol
		{
			for (int i = 0; i < newarr.length; i++)
			{
				newarr[i] = arr[2*i];
			}
		}

		else //odd protocol
		{
			for (int i = 0; i < newarr.length; i++)
			{
				newarr[i] = arr[(2*i)+1];
			}
		}

		return newarr;
	}

	// This method perfroms the ntt operation on a 64 length array; computes A(omega)
	private static int ntt(int[] arr, int omega)
	{
		if (arr.length > 1) //recursive step
		{
			int even[] = filter (2, arr);
			int odd[] = filter (1, arr);
			return ((ntt(even, (omega * omega) % 257) + (omega*(ntt(odd, (omega * omega) % 257)))) % 257);
		}

		else //base case
		{
			return (arr[0] * omega) % 257;
		}
	}

	// This is the method that will be called to perform SWIFFT. It pads the array if necessary, checks the matrix,
	// and returns z in an array
	public static int[] swifft(int[] arr, int[][] mat)
	{
		if ((arr.length % 64) != 0) //performs array padding
		{
			int[] newarr = new int[arr.length + (64 - (arr.length % 64))];
			for (int i = 0; i < arr.length; i++)
			{
				newarr[i] = arr[i];
			}
			return swifft(newarr, mat);
		}
		
		try {  
				int x = mat[(arr.length / 64)-1][63]; //check to see if matrix is valid
		}

		catch (IndexOutOfBoundsException e) {
				System.out.println("Matrix is too small. Must at least be Matrix[Array.length / 64 - 1][64]");
				System.exit(1);
		}

		int[] bitreverse = {0,32,16,48,8,40,24,56,4,36,20,52,12,44,28,60,2,34,18,50,10,42,26,58,6,38,22,54,14,46,
							30,62,1,33,17,49,9,41,25,57,5,37,21,53,13,45,29,61,3,35,19,51,11,43,27,59,7,39,23,55,15,
								47,31,63}; //Pre-computed bitreversal values; index 2 value becomes index 32 value

		int[] w = {1,42,222,72,197,50,44,49,2,84,187,144,137,100,88,98,4,168,117,31,17,200,176,196,8,79,234,62,34,
					143,95,135,16,158,211,124,68,29,190,13,32,59,165,248,136,58,123,26,64,118,73,239,15,116,246,52,128,
						236,146,221,30,232,235,104,256}; //Pre-computed values of w^0 - w^64 mod 257

		int[] w2 = {1,222,197,44,2,187,137,88,4,117,17,176,8,234,34,95,16,211,68,190,32,165,136,123,64,73,15,246,128,
					146,30,235,256,35,60,213,255,70,120,169,253,140,240,81,249,23,223,162,241,46,189,67,225,92,121,134,193,
						184,242,11,129,111,227,22}; //Pre-computed values of w_squared^0 - w_squared^64 mod 257

		int[] temp_arr = new int[64]; //stores subsection of input message
		int[] br_arr = new int[64]; //stores array post bitreversal/premultiply
		int[] z = new int[64]; //this will be returned at the end

		//SWIFFT
		for (int i = 0; i < (arr.length / 64); i++) //loops through length of message
		{
			temp_arr = Arrays.copyOfRange(arr, (64*i), ((64*i)+64)); //grab current 64 length section of the message 

			
				br_arr[0] = temp_arr[bitreverse[0]]; //bitreverse
				br_arr[0] = (br_arr[0] * w[0] % 257); //premultiply
				br_arr[1] = temp_arr[bitreverse[1]]; 
				br_arr[1] = (br_arr[1] * w[1] % 257); 
				br_arr[2] = temp_arr[bitreverse[2]]; 
				br_arr[2] = (br_arr[2] * w[2] % 257); 
				br_arr[3] = temp_arr[bitreverse[3]]; 
				br_arr[3] = (br_arr[3] * w[3] % 257); 
				br_arr[4] = temp_arr[bitreverse[4]]; 
				br_arr[4] = (br_arr[4] * w[4] % 257); 
				br_arr[5] = temp_arr[bitreverse[5]]; 
				br_arr[5] = (br_arr[5] * w[5] % 257); 
				br_arr[6] = temp_arr[bitreverse[6]]; 
				br_arr[6] = (br_arr[6] * w[6] % 257); 
				br_arr[7] = temp_arr[bitreverse[7]]; 
				br_arr[7] = (br_arr[7] * w[7] % 257); 
				br_arr[8] = temp_arr[bitreverse[8]]; 
				br_arr[8] = (br_arr[8] * w[8] % 257); 
				br_arr[9] = temp_arr[bitreverse[9]]; 
				br_arr[9] = (br_arr[9] * w[9] % 257); 
				br_arr[10] = temp_arr[bitreverse[10]]; 
				br_arr[10] = (br_arr[10] * w[10] % 257); 
				br_arr[11] = temp_arr[bitreverse[11]]; 
				br_arr[11] = (br_arr[11] * w[11] % 257); 
				br_arr[12] = temp_arr[bitreverse[12]]; 
				br_arr[12] = (br_arr[12] * w[12] % 257); 
				br_arr[13] = temp_arr[bitreverse[13]]; 
				br_arr[13] = (br_arr[13] * w[13] % 257); 
				br_arr[14] = temp_arr[bitreverse[14]]; 
				br_arr[14] = (br_arr[14] * w[14] % 257); 
				br_arr[15] = temp_arr[bitreverse[15]]; 
				br_arr[15] = (br_arr[15] * w[15] % 257); 
				br_arr[16] = temp_arr[bitreverse[16]]; 
				br_arr[16] = (br_arr[16] * w[16] % 257); 
				br_arr[17] = temp_arr[bitreverse[17]]; 
				br_arr[17] = (br_arr[17] * w[17] % 257); 
				br_arr[18] = temp_arr[bitreverse[18]]; 
				br_arr[18] = (br_arr[18] * w[18] % 257); 
				br_arr[19] = temp_arr[bitreverse[19]]; 
				br_arr[19] = (br_arr[19] * w[19] % 257); 
				br_arr[20] = temp_arr[bitreverse[20]]; 
				br_arr[20] = (br_arr[20] * w[20] % 257); 
				br_arr[21] = temp_arr[bitreverse[21]]; 
				br_arr[21] = (br_arr[21] * w[21] % 257); 
				br_arr[22] = temp_arr[bitreverse[22]]; 
				br_arr[22] = (br_arr[22] * w[22] % 257); 
				br_arr[23] = temp_arr[bitreverse[23]]; 
				br_arr[23] = (br_arr[23] * w[23] % 257); 
				br_arr[24] = temp_arr[bitreverse[24]]; 
				br_arr[24] = (br_arr[24] * w[24] % 257); 
				br_arr[25] = temp_arr[bitreverse[25]]; 
				br_arr[25] = (br_arr[25] * w[25] % 257); 
				br_arr[26] = temp_arr[bitreverse[26]]; 
				br_arr[26] = (br_arr[26] * w[26] % 257); 
				br_arr[27] = temp_arr[bitreverse[27]]; 
				br_arr[27] = (br_arr[27] * w[27] % 257); 
				br_arr[28] = temp_arr[bitreverse[28]]; 
				br_arr[28] = (br_arr[28] * w[28] % 257); 
				br_arr[29] = temp_arr[bitreverse[29]]; 
				br_arr[29] = (br_arr[29] * w[29] % 257); 
				br_arr[30] = temp_arr[bitreverse[30]]; 
				br_arr[30] = (br_arr[30] * w[30] % 257); 
				br_arr[31] = temp_arr[bitreverse[31]]; 
				br_arr[31] = (br_arr[31] * w[31] % 257); 
				br_arr[32] = temp_arr[bitreverse[32]]; 
				br_arr[32] = (br_arr[32] * w[32] % 257); 
				br_arr[33] = temp_arr[bitreverse[33]]; 
				br_arr[33] = (br_arr[33] * w[33] % 257); 
				br_arr[34] = temp_arr[bitreverse[34]]; 
				br_arr[34] = (br_arr[34] * w[34] % 257); 
				br_arr[35] = temp_arr[bitreverse[35]]; 
				br_arr[35] = (br_arr[35] * w[35] % 257); 
				br_arr[36] = temp_arr[bitreverse[36]]; 
				br_arr[36] = (br_arr[36] * w[36] % 257); 
				br_arr[37] = temp_arr[bitreverse[37]]; 
				br_arr[37] = (br_arr[37] * w[37] % 257); 
				br_arr[38] = temp_arr[bitreverse[38]]; 
				br_arr[38] = (br_arr[38] * w[38] % 257); 
				br_arr[39] = temp_arr[bitreverse[39]]; 
				br_arr[39] = (br_arr[39] * w[39] % 257); 
				br_arr[40] = temp_arr[bitreverse[40]]; 
				br_arr[40] = (br_arr[40] * w[40] % 257); 
				br_arr[41] = temp_arr[bitreverse[41]]; 
				br_arr[41] = (br_arr[41] * w[41] % 257); 
				br_arr[42] = temp_arr[bitreverse[42]]; 
				br_arr[42] = (br_arr[42] * w[42] % 257); 
				br_arr[43] = temp_arr[bitreverse[43]]; 
				br_arr[43] = (br_arr[43] * w[43] % 257); 
				br_arr[44] = temp_arr[bitreverse[44]]; 
				br_arr[44] = (br_arr[44] * w[44] % 257); 
				br_arr[45] = temp_arr[bitreverse[45]]; 
				br_arr[45] = (br_arr[45] * w[45] % 257); 
				br_arr[46] = temp_arr[bitreverse[46]]; 
				br_arr[46] = (br_arr[46] * w[46] % 257); 
				br_arr[47] = temp_arr[bitreverse[47]]; 
				br_arr[47] = (br_arr[47] * w[47] % 257); 
				br_arr[48] = temp_arr[bitreverse[48]]; 
				br_arr[48] = (br_arr[48] * w[48] % 257); 
				br_arr[49] = temp_arr[bitreverse[49]]; 
				br_arr[49] = (br_arr[49] * w[49] % 257); 
				br_arr[50] = temp_arr[bitreverse[50]]; 
				br_arr[50] = (br_arr[50] * w[50] % 257); 
				br_arr[51] = temp_arr[bitreverse[51]]; 
				br_arr[51] = (br_arr[51] * w[51] % 257); 
				br_arr[52] = temp_arr[bitreverse[52]]; 
				br_arr[52] = (br_arr[52] * w[52] % 257); 
				br_arr[53] = temp_arr[bitreverse[53]]; 
				br_arr[53] = (br_arr[53] * w[53] % 257); 
				br_arr[54] = temp_arr[bitreverse[54]]; 
				br_arr[54] = (br_arr[54] * w[54] % 257); 
				br_arr[55] = temp_arr[bitreverse[55]]; 
				br_arr[55] = (br_arr[55] * w[55] % 257); 
				br_arr[56] = temp_arr[bitreverse[56]]; 
				br_arr[56] = (br_arr[56] * w[56] % 257); 
				br_arr[57] = temp_arr[bitreverse[57]]; 
				br_arr[57] = (br_arr[57] * w[57] % 257); 
				br_arr[58] = temp_arr[bitreverse[58]]; 
				br_arr[58] = (br_arr[58] * w[58] % 257); 
				br_arr[59] = temp_arr[bitreverse[59]]; 
				br_arr[59] = (br_arr[59] * w[59] % 257); 
				br_arr[60] = temp_arr[bitreverse[60]]; 
				br_arr[60] = (br_arr[60] * w[60] % 257); 
				br_arr[61] = temp_arr[bitreverse[61]]; 
				br_arr[61] = (br_arr[61] * w[61] % 257); 
				br_arr[62] = temp_arr[bitreverse[62]]; 
				br_arr[62] = (br_arr[62] * w[62] % 257); 
				br_arr[63] = temp_arr[bitreverse[63]]; 
				br_arr[63] = (br_arr[63] * w[63] % 257); 
			

			
			
				z[0] = ((z[0] + (mat[i][0] * ntt(br_arr, w2[0]))) % 257); //compute z
				z[1] = ((z[1] + (mat[i][1] * ntt(br_arr, w2[1]))) % 257);
				z[2] = ((z[2] + (mat[i][2] * ntt(br_arr, w2[2]))) % 257);
				z[3] = ((z[3] + (mat[i][3] * ntt(br_arr, w2[3]))) % 257);
				z[4] = ((z[4] + (mat[i][4] * ntt(br_arr, w2[4]))) % 257);
				z[5] = ((z[5] + (mat[i][5] * ntt(br_arr, w2[5]))) % 257);
				z[6] = ((z[6] + (mat[i][6] * ntt(br_arr, w2[6]))) % 257);
				z[7] = ((z[7] + (mat[i][7] * ntt(br_arr, w2[7]))) % 257);
				z[8] = ((z[8] + (mat[i][8] * ntt(br_arr, w2[8]))) % 257);
				z[9] = ((z[9] + (mat[i][9] * ntt(br_arr, w2[9]))) % 257);
				z[10] = ((z[10] + (mat[i][10] * ntt(br_arr, w2[10]))) % 257);
				z[11] = ((z[11] + (mat[i][11] * ntt(br_arr, w2[11]))) % 257);
				z[12] = ((z[12] + (mat[i][12] * ntt(br_arr, w2[12]))) % 257);
				z[13] = ((z[13] + (mat[i][13] * ntt(br_arr, w2[13]))) % 257);
				z[14] = ((z[14] + (mat[i][14] * ntt(br_arr, w2[14]))) % 257);
				z[15] = ((z[15] + (mat[i][15] * ntt(br_arr, w2[15]))) % 257);
				z[16] = ((z[16] + (mat[i][16] * ntt(br_arr, w2[16]))) % 257);
				z[17] = ((z[17] + (mat[i][17] * ntt(br_arr, w2[17]))) % 257);
				z[18] = ((z[18] + (mat[i][18] * ntt(br_arr, w2[18]))) % 257);
				z[19] = ((z[19] + (mat[i][19] * ntt(br_arr, w2[19]))) % 257);
				z[20] = ((z[20] + (mat[i][20] * ntt(br_arr, w2[20]))) % 257);
				z[21] = ((z[21] + (mat[i][21] * ntt(br_arr, w2[21]))) % 257);
				z[22] = ((z[22] + (mat[i][22] * ntt(br_arr, w2[22]))) % 257);
				z[23] = ((z[23] + (mat[i][23] * ntt(br_arr, w2[23]))) % 257);
				z[24] = ((z[24] + (mat[i][24] * ntt(br_arr, w2[24]))) % 257);
				z[25] = ((z[25] + (mat[i][25] * ntt(br_arr, w2[25]))) % 257);
				z[26] = ((z[26] + (mat[i][26] * ntt(br_arr, w2[26]))) % 257);
				z[27] = ((z[27] + (mat[i][27] * ntt(br_arr, w2[27]))) % 257);
				z[28] = ((z[28] + (mat[i][28] * ntt(br_arr, w2[28]))) % 257);
				z[29] = ((z[29] + (mat[i][29] * ntt(br_arr, w2[29]))) % 257);
				z[30] = ((z[30] + (mat[i][30] * ntt(br_arr, w2[30]))) % 257);
				z[31] = ((z[31] + (mat[i][31] * ntt(br_arr, w2[31]))) % 257);
				z[32] = ((z[32] + (mat[i][32] * ntt(br_arr, w2[32]))) % 257);
				z[33] = ((z[33] + (mat[i][33] * ntt(br_arr, w2[33]))) % 257);
				z[34] = ((z[34] + (mat[i][34] * ntt(br_arr, w2[34]))) % 257);
				z[35] = ((z[35] + (mat[i][35] * ntt(br_arr, w2[35]))) % 257);
				z[36] = ((z[36] + (mat[i][36] * ntt(br_arr, w2[36]))) % 257);
				z[37] = ((z[37] + (mat[i][37] * ntt(br_arr, w2[37]))) % 257);
				z[38] = ((z[38] + (mat[i][38] * ntt(br_arr, w2[38]))) % 257);
				z[39] = ((z[39] + (mat[i][39] * ntt(br_arr, w2[39]))) % 257);
				z[40] = ((z[40] + (mat[i][40] * ntt(br_arr, w2[40]))) % 257);
				z[41] = ((z[41] + (mat[i][41] * ntt(br_arr, w2[41]))) % 257);
				z[42] = ((z[42] + (mat[i][42] * ntt(br_arr, w2[42]))) % 257);
				z[43] = ((z[43] + (mat[i][43] * ntt(br_arr, w2[43]))) % 257);
				z[44] = ((z[44] + (mat[i][44] * ntt(br_arr, w2[44]))) % 257);
				z[45] = ((z[45] + (mat[i][45] * ntt(br_arr, w2[45]))) % 257);
				z[46] = ((z[46] + (mat[i][46] * ntt(br_arr, w2[46]))) % 257);
				z[47] = ((z[47] + (mat[i][47] * ntt(br_arr, w2[47]))) % 257);
				z[48] = ((z[48] + (mat[i][48] * ntt(br_arr, w2[48]))) % 257);
				z[49] = ((z[49] + (mat[i][49] * ntt(br_arr, w2[49]))) % 257);
				z[50] = ((z[50] + (mat[i][50] * ntt(br_arr, w2[50]))) % 257);
				z[51] = ((z[51] + (mat[i][51] * ntt(br_arr, w2[51]))) % 257);
				z[52] = ((z[52] + (mat[i][52] * ntt(br_arr, w2[52]))) % 257);
				z[53] = ((z[53] + (mat[i][53] * ntt(br_arr, w2[53]))) % 257);
				z[54] = ((z[54] + (mat[i][54] * ntt(br_arr, w2[54]))) % 257);
				z[55] = ((z[55] + (mat[i][55] * ntt(br_arr, w2[55]))) % 257);
				z[56] = ((z[56] + (mat[i][56] * ntt(br_arr, w2[56]))) % 257);
				z[57] = ((z[57] + (mat[i][57] * ntt(br_arr, w2[57]))) % 257);
				z[58] = ((z[58] + (mat[i][58] * ntt(br_arr, w2[58]))) % 257);
				z[59] = ((z[59] + (mat[i][59] * ntt(br_arr, w2[59]))) % 257);
				z[60] = ((z[60] + (mat[i][60] * ntt(br_arr, w2[60]))) % 257);
				z[61] = ((z[61] + (mat[i][61] * ntt(br_arr, w2[61]))) % 257);
				z[62] = ((z[62] + (mat[i][62] * ntt(br_arr, w2[62]))) % 257);
				z[63] = ((z[63] + (mat[i][63] * ntt(br_arr, w2[63]))) % 257);
			
		}

		return z; 
	}
}