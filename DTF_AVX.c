//In the makefile, you need to add a flag to the compile instruction to include AVX2
 

void DFT(int input[N], int output[N]) 
{
	int k, i;
	for (i = 0; i < N; i++) 
	{
    float guess = 0;

		for (k = 0; k < N; k+=8) 
		{
			// The 2 is here because omega^2 is a primitive 64'th root in our field.
      int power = (2*i); 

      __m256 vector1 = _mm256_setr_ps((float)input[k], (float)input[k+1], (float)input[k+2],  // load 8 elements from input
          (float)input[k+3], (float)input[k+4], (float)input[k+5], (float)input[k+6], (float)input[k+7]);
      __m256 vector2 = _mm256_setr_ps((float)omegaPowers[(power*k) % (128)], (float)omegaPowers[(power*(k+1)) % (128)], //load 8 elements from omegapoers
        (float)omegaPowers[(power*(k+2)) % (128)], (float)omegaPowers[(power*(k+3)) % (128)], 
          (float)omegaPowers[(power*(k+4)) % (128)], (float)omegaPowers[(power*(k+5)) % (128)], 
          (float)omegaPowers[(power*(k+6)) % (128)], (float)omegaPowers[(power*(k+7)) % (128)]);
      __m256 vector3 = _mm256_mul_ps(vector1, vector2); //vector multiply
      __m128 hiQuad = _mm256_extractf128_ps(vector3, 1); // Horizontal sum of vector 3 *
      __m128 loQuad = _mm256_castps256_ps128(vector3); // *
      __m128 sumQuad = _mm_add_ps(loQuad, hiQuad); // *
      __m128 loDual = sumQuad; // *
      __m128 hiDual = _mm_movehl_ps(sumQuad, sumQuad); // *
      __m128 sumDual = _mm_add_ps(loDual, hiDual); // *
      __m128 lo = sumDual; // *
      __m128 hi = _mm_shuffle_ps(sumDual, sumDual, 0x1); //*
      __m128 sum2 = _mm_add_ss(lo, hi); //*
      guess =  guess + _mm_cvtss_f32(sum2); // sum of all vectors within loop
		}

		output[i] = (int)guess % FIELD_SIZE;
	}
}