(* 
	*  Patrick Woods
	*  07/18/2017
	*  Envieta Systems LLC
	*  SWIFFT OCaml Implementation

	This program contains several functions that are used together to form the SWIFFT compression function, 
	which is called by the final function swifft. I have yet to make any specific optimizations, and I did not 
	make it a single call tail-recursive function, but both can be done. The final function swifft takes 2 
	parameters, a binary inputmessage, and a matrix of at least (inputmessage.length)/64 x 64. The binary 
	message can be any length, however if the length is not divisable by 64, then 0's will be padded to the
	end of it until it is divisable by 64. The function outputs the 64 computed 'z' values in modulo 257. 
	Modular arithmetic is used thoroughly during the calculation to avoid an int overflow. 

 *)

open NTT;;
open Array;;

(* Initialization of output z array, length 64 filled with 0's to start *)
let z = make 64 0;;

(* Index Bit Reverse Algo *********** *)
(* These 2 perform the IndexBitReversal algorithm. irev takes a value x and returns x', or x with its bits reversed. The other two 
parameters are used to store information necessary for the calculation/return of the computed value. bitreverse takes an array and makes
the appropiate swap of each element based on its index's reversed bits *)
let rec irev x j final = let i = x land j in if j == 1 then irev x 2 (final + (i*32)) else if j == 2 then irev x 4 (final + (i*8)) else if j == 4 then irev x 8 (final + (i*2))
					else if j == 8 then irev x 16 (final + (i/2)) else if j == 16 then irev x 32 (final + (i/8)) else if j == 32 then irev x 999 (final + (i/32)) else final;;
let bitreverse arr = mapi (fun i x -> arr.(irev i 1 0)) arr;;
(* *********** Index Bit Reverse Algo *)

(* This takes an int x and returns 42^x mod 257 *)
let rec fac x o = if x > 0 then fac (x-1) (42*o mod 257) else o;; 

(* This simply takes a value x, index j, and updates z[j] with x. I compute the updated value of z before hand, and pass it into this function.
This is necessary because OCaml gives me an error when trying to access the value in z while also trying to change the value in z *)
let zi x j = z.(j) <- (x mod 257);;

(* This takes an array of values and adds them to the previous values of z *)
let zupdate arr = iteri (fun i x -> zi ((z.(i) + x) mod 257) i) arr;;

(* This performs the SWIFFT function. It passes the bitreversed message into NTT after multiplying the ith indexed terms by 42^i. It then takes the resulting
digest and multiplies each ith term by the ith term in the matrix. This final array is then used to update the values of z. *)
let rec swifft_inner message matrix = zupdate (mapi (fun i x -> (matrix.(i)*x) mod 257) (ntt (mapi (fun i x -> (x*(fac 1 i)) mod 257) (bitreverse message))));;

(* This loops through the input message 64 numbers at a time, feeding the 64 numbers and the appropiate branch of the matrix into the SWIFFT function  *)
let rec swifft_outer message matrix i k = if i < k then (swifft_inner (sub message (i*64) 64) matrix.(i); swifft_outer message matrix (i+1) k) else z;;

(* This takes an int and incremenets it until it is 0 mod 64. Used for input padding *)
let rec kupdate k = if k mod 64 != 0 then kupdate (k+1) else k;;

(* This uses kupdate to build the appropiate sized binary array (padded with 0's if necessary) to be passed into swifft_ *)
let amaker k inp = mapi (fun i y -> if i < length inp then inp.(i) else 0) (make k 0);;

(* This is to be called by the user to use the SWIFFT function. It takes any binary input array and matrix and prepares them (pads array if length % 64 != 0,
rejects if matrix is not array.length / 64 x 64) to be used for swifft. *)
let swifft inp m = let k = kupdate (length inp) in if k/64 <= length m then if length m.(0) == 64 then swifft_outer (amaker k inp) m 0 (k/64) else 
	z else z;;




	(* EOF *)