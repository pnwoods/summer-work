(* 
	*  Patrick Woods
	*  07/18/2017
	*  Envieta Systems LLC
	*  SWIFFT OCaml Implementation

	This program tests the SWIFFT and NTT functions with a variety of inputs. There are labeled sections,
	including one where you can manually test specific inputs. To compile, simply type: ocamlc -o fft NTT.ml swifft_ntt.ml testswifft.ml 
	To then run, type ./fft

 *)
open NTT;;
open Swifft_ntt;;
open Random;;
open Printf;;
open Array;;
open Unix;;

(* Initializes random number generator *)
self_init ();;

(* Function used for benchmark *)
 let time f =
    let t = Unix.gettimeofday () in
    let fx = f in
    Printf.printf "execution elapsed time: %f sec\n"
        (Unix.gettimeofday () -. t);
    fx 

(* This generates a matrix of any size with random values mod 257, and prints out the values *)
let mmaker mat = 
	for i = 0 to (length mat) - 1 do 
		printf "Matrix[%d]: " i;
		for j = 0 to (length mat.(0) - 1) do 
			(mat.(i).(j) <- Random.int 257); printf "%d " mat.(i).(j)
		done;
		printf "\n"
	done
;;

(* VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV *)

(* This test method allows you to manually test the program with personalized inputs. Simply change the value on
   the right side of '<-' symbol to change the value of the desired index. *)
(* ************** *)
printf "(* This test method allows you to manually test the program with personalized inputs. Simply change the value on 
the right side of '<-' symbol to change the value of the desired index. *)\n";;
let user_test = 
let message = make 64 0 in
let matrix = make_matrix 1 64 0 in 
message.(0) <- 0; message.(1) <- 1; message.(2) <- 0;
message.(3) <- 0; message.(4) <- 0; message.(5) <- 1; 
message.(6) <- 0; message.(7) <- 0; message.(8) <- 0;
message.(9) <- 0; message.(10) <- 0; message.(11) <- 0;
message.(12) <- 0; message.(13) <- 0; message.(14) <- 0;
message.(15) <- 0; message.(16) <- 0; message.(17) <- 0;
message.(18) <- 0; message.(19) <- 0; message.(20) <- 0;
message.(21) <- 0; message.(22) <- 0; message.(23) <- 0;
message.(24) <- 0; message.(25) <- 0; message.(26) <- 0;
message.(27) <- 0; message.(28) <- 0; message.(29) <- 0;
message.(30) <- 0; message.(31) <- 0; message.(32) <- 0;
message.(33) <- 0; message.(34) <- 1; message.(35) <- 0;
message.(36) <- 0; message.(37) <- 0; message.(38) <- 0;
message.(39) <- 0; message.(40) <- 0; message.(41) <- 0;
message.(42) <- 0; message.(43) <- 0; message.(44) <- 0;
message.(45) <- 0; message.(46) <- 0; message.(47) <- 0;
message.(48) <- 0; message.(49) <- 0; message.(50) <- 0;
message.(51) <- 0; message.(52) <- 0; message.(53) <- 0;
message.(54) <- 0; message.(55) <- 0; message.(56) <- 1;
message.(57) <- 0; message.(58) <- 0; message.(59) <- 0;
message.(60) <- 0; message.(61) <- 0; message.(62) <- 0;
message.(63) <- 0;

matrix.(0).(0) <- 188; matrix.(0).(1) <- 132; matrix.(0).(2) <- 249;
matrix.(0).(3) <- 204; matrix.(0).(4) <- 69; matrix.(0).(5) <- 166; 
matrix.(0).(6) <- 164; matrix.(0).(7) <- 50; matrix.(0).(8) <- 88;
matrix.(0).(9) <- 78; matrix.(0).(10) <- 106; matrix.(0).(11) <- 11;
matrix.(0).(12) <- 1; matrix.(0).(13) <- 202; matrix.(0).(14) <- 59;
matrix.(0).(15) <- 201; matrix.(0).(16) <- 208; matrix.(0).(17) <- 245;
matrix.(0).(18) <- 155; matrix.(0).(19) <- 55; matrix.(0).(20) <- 141;
matrix.(0).(21) <- 238; matrix.(0).(22) <- 69; matrix.(0).(23) <- 23;
matrix.(0).(24) <- 60; matrix.(0).(25) <- 175; matrix.(0).(26) <- 228;
matrix.(0).(27) <- 86; matrix.(0).(28) <- 76; matrix.(0).(29) <- 193;
matrix.(0).(30) <- 193; matrix.(0).(31) <- 47; matrix.(0).(32) <- 104;
matrix.(0).(33) <- 80; matrix.(0).(34) <- 87; matrix.(0).(35) <- 220;
matrix.(0).(36) <- 80; matrix.(0).(37) <- 34; matrix.(0).(38) <- 244;
matrix.(0).(39) <- 168; matrix.(0).(40) <- 3; matrix.(0).(41) <- 216;
matrix.(0).(42) <- 185; matrix.(0).(43) <- 122; matrix.(0).(44) <- 169;
matrix.(0).(45) <- 32; matrix.(0).(46) <- 128; matrix.(0).(47) <- 97;
matrix.(0).(48) <- 106; matrix.(0).(49) <- 177; matrix.(0).(50) <- 222;
matrix.(0).(51) <- 101; matrix.(0).(52) <- 72; matrix.(0).(53) <- 30;
matrix.(0).(54) <- 135; matrix.(0).(55) <- 104; matrix.(0).(56) <- 251;
matrix.(0).(57) <- 184; matrix.(0).(58) <- 15; matrix.(0).(59) <- 135;
matrix.(0).(60) <- 69; matrix.(0).(61) <- 21; matrix.(0).(62) <- 102;
matrix.(0).(63) <- 146;
printf "\nInput matrix: \n\n";
let () = iter (printf "%d ") matrix.(0) in
printf "\n\nInput message: \n\n";
let () = iter (printf "%d ") message in
printf "\n\nInput message post-bitreversal: \n\n";
let br = bitreverse message in
let () = iter (printf "%d ") br in
printf "\n\nBit-reversed message after omega^i pre-multiply: \n\n";
let messagemult = mapi (fun i x -> (br.(i) * (fac i 1)) mod 257) br in
let () = iter (printf "%d ") messagemult in
printf "\n\nBit-reversed/pre-multiplied message after NTT: \n\n";
let nm = ntt messagemult in
let () = iter (printf "%d ") nm in
printf "\n\nBit-reversed/pre-multiplied/ntt message after matrix post-multiply (a.k.a z): \n\n";
let () = iter (printf "%d ") (mapi (fun i x -> (nm.(i) * matrix.(0).(i)) mod 257) nm) in
printf "\n\n****************************************************************\n";;

(* ************** *)


(* This section of test methods demonstrates the SWIFFT program with random inputs for m = 1, 2, 3, 4, and 32 *)
(* ************** *)
printf "(* This section of test methods demonstrates the SWIFFT program with random inputs for m = 1, 2, 3, 4, and 32 *)\n\n";;
let test1 = 
printf "m = 1 \n\n";
printf "The input matrix: \n\n";
let matrix = make_matrix 1 64 0 in
mmaker matrix;
printf "\n\n";
let message = Array.map (fun x -> Random.int 2) (make 64 0) in
printf "The input message: \n\n";
let () = iter (printf "%d ") message in
printf "\n\n";
printf "The output z: \n\n";
let () = iter (printf "%d ") (swifft message matrix) in
printf "\n";
printf "\n(* Benchmark for 64 length input message *)\n";
time (swifft message matrix);
printf "\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ \n" ;;

let test2 = 
printf "m = 2 \n\n";
printf "The input matrix: \n\n";
let matrix = make_matrix 2 64 0 in
mmaker matrix;
printf "\n\n";
let message = Array.map (fun x -> Random.int 2) (make (64*2) 0) in
printf "The input message: \n\n";
let () = iter (printf "%d ") message in
printf "\n\n";
printf "The output z: \n\n";
let () = iter (printf "%d ") (swifft message matrix) in
printf "\n";
printf "\n(* Benchmark for 2*64 length input message *)\n";
time (swifft message matrix);
printf "\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ \n" ;;

let test3 = 
printf "m = 3 \n\n";
printf "The input matrix: \n\n";
let matrix = make_matrix 3 64 0 in
mmaker matrix;
printf "\n\n";
let message = Array.map (fun x -> Random.int 2) (make (64*3) 0) in
printf "The input message: \n\n";
let () = iter (printf "%d ") message in
printf "\n\n";
printf "The output z: \n\n";
let () = iter (printf "%d ") (swifft message matrix) in
printf "\n";
printf "\n(* Benchmark for 3*64 length input message *)\n";
time (swifft message matrix);
printf "\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ \n" ;;

let test4 = 
printf "m = 4 \n\n";
printf "The input matrix: \n\n";
let matrix = make_matrix 4 64 0 in
mmaker matrix;
printf "\n\n";
let message = Array.map (fun x -> Random.int 2) (make (64*4) 0) in
printf "The input message: \n\n";
let () = iter (printf "%d ") message in
printf "\n\n";
printf "The output z: \n\n";
let () = iter (printf "%d ") (swifft message matrix) in
printf "\n";
printf "\n(* Benchmark for 4*64 length input message *)\n";
time (swifft message matrix);
printf "\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ \n" ;;

let test5 = 
printf "m = 32 \n\n";
printf "The input matrix: \n\n";
let matrix = make_matrix 32 64 0 in
mmaker matrix;
printf "\n\n";
let message = Array.map (fun x -> Random.int 2) (make (64*32) 0) in
printf "The input message: \n\n";
let () = iter (printf "%d ") message in
printf "\n\n";
printf "The output z: \n\n";
let () = iter (printf "%d ") (swifft message matrix) in
printf "\n";
printf "\n(* Benchmark for 32*64 length input message *)\n";
time (swifft message matrix);
printf "\n\n**************************************************************** \n" ;

(* ************** *)


(* This test method demonstrates the NTT roots of unity property that I described in the email *)
(* ************** *)
printf "\n(* This test method demonstrates the NTT roots of unity property that I described in the email *)\n\n";;
let unity_test k =
let message = make 64 0 in
message.(k) <- 1;
printf "%d: " k;
iter (printf "%d ") (ntt message);
printf "\n\n";
in iteri (fun i x -> unity_test i) (make 64 0); printf "**************************************************************** \n" ;;

(* ************** *)


(* This section of test methods demonstrates the SWIFFT program with invalid input parameters *)
(* ************** *)
printf "(* This section of test methods demonstrates the SWIFFT program with invalid input parameters *)\n\n";;
let fail_test1 =
let message = make 5 0 in
printf "Input message (invalid): \n\n";
iter (printf "%d ") message;
printf "\n\nInput matrix (valid): \n\n";
let matrix = make_matrix 1 64 0 in
mmaker matrix;
printf "\n\nz: \n\n";
iter (printf "%d ") (swifft message matrix); printf "\n\n";;

let fail_test2 =
let message = make 70 0 in
printf "Input message (invalid, 70 mod 64 != 0): \n\n";
iter (printf "%d ") message;
printf "\n\nInput matrix (valid): \n\n";
let matrix = make_matrix 1 64 0 in
mmaker matrix;
printf "\n\nz: \n\n";
iter (printf "%d ") (swifft message matrix); printf "\n\n";;

let fail_test3 =
let message = make (64*2) 1 in
printf "Input message (valid): \n\n";
iter (printf "%d ") message;
printf "\n\nInput matrix (invalid, small): \n\n";
let matrix = make_matrix 1 64 0 in
mmaker matrix;
printf "\n\nz: \n\n";
iter (printf "%d ") (swifft message matrix); printf "\n\n";;

let fail_test4 =
let message = make 64 0 in
printf "Input message (valid): \n\n";
iter (printf "%d ") message;
printf "\n\nInput matrix (invalid, large): \n\n";
let matrix = make_matrix 2 64 0 in
mmaker matrix;
printf "\nz: \n\n";
iter (printf "%d ") (swifft message matrix); printf "\n\n";;
(* ************** *)





(* EOF *)