(* 
	*  Patrick Woods
	*  07/18/2017
	*  Envieta Systems LLC
	*  SWIFFT OCaml Implementation

	This program performs the NTT algorithm using w^2 (w = 42) as the root of unity.
	Given an array of 64 values, it returns the array (polynomial) evaluated at
	all powers of w^2. In other words, A[(w^2)^0]; A[(w^2)^1]; ... A{(w^2)^63}. 
	These 64 values are returned in an array

 *)

open Array;;

(* Our root of unity (omega = 42, but we are finding points for all powers of omega squared) *)
let w2 = (42*42) mod 257;;

(* This function takes in our omega squared value and returns the desired power of it in mod 257 *)
let rec fac o n = if n == 0 then 1 else if n == 1 then o else fac ((o*w2) mod 257) (n - 1);;

(* This function takes in an array and an int. If the int is odd then this function returns all of the odd elements, and likewise for evens *)
let filter arr x = let temp_arr = (make ((length arr)/2) 0) in if x mod 2 == 0 then mapi (fun i q -> arr.(i*2)) temp_arr 
					else mapi (fun i q -> arr.((i*2)+1)) temp_arr
;;

(* This function performs the NTT operation. Given an array and an omega, it computes A[omega] in NTT fashion *)
let ntt_ arr omega = 
	let rec ntt__ message o = if length message == 1 then (message.(0) * o) else 
	let even = ntt__ (filter message 2) (o*o mod 257) in
	let odd = ntt__ (filter message 1) (o*o mod 257) in
	(even + (o * odd)) mod 257 in
	ntt__ arr omega
;;

(* This function calls ntt_ as it loops through all powers of omega squared. Returns the 'y' vector *)
let ntt arr = mapi (fun i x -> ntt_ arr (fac w2 i)) (make 64 0);;



(* EOF *)